Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s1-0009-ourplant_x3_blue_2).

| document | download options |
|:-------- | ----------------:|
| operating manual           |[de](https://gitlab.com/ourplant.net/products/s1-0009-ourplant_x3_blue_2/-/raw/main/01_operating_manual/S1-0008-0011_D1_BA_OurPlant%20X3.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s1-0009-ourplant_x3_blue_2/-/raw/main/02_assembly_drawing/s1-0009_D_ZNB_ourplant_x3_blue2.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s1-0009-ourplant_x3_blue_2/-/raw/main/03_circuit_diagram/S1-0008_to_S1-0011_A_EPLAN_OurPlant%20X3.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s1-0009-ourplant_x3_blue_2/-/raw/main/05_spare_parts/S1-0009_B_EVL_OurPlant%20X3%20.pdf), [en](https://gitlab.com/ourplant.net/products/s1-0009-ourplant_x3_blue_2/-/raw/main/05_spare_parts/S1-0009_B_SWP_OurPlant-X3.pdf)|

